// Copyright � 2006-2010 Travis Robinson. All rights reserved.
// 
// website: http://sourceforge.net/projects/libusbdotnet
// e-mail:  libusbdotnet@gmail.com
// 
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License, or 
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. or 
// visit www.gnu.org.
// 
// 
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Management;

namespace LibUsbDotNet.DeviceNotify
{
    /// <summary>
    /// Notifies an application of a change to the hardware Configuration of a device or 
    /// the computer. See <see cref="IDeviceNotifier"/> or <see cref="DeviceNotifier.OpenDeviceNotifier"/> interface for more information
    /// </summary>
    /// <remarks>
    /// This is the windows implementation of the device notifier.
    /// </remarks>
    public class WindowsManagementUSBDeviceNotifier : IDeviceNotifier
    {
        private bool mEnabled = false;
        static readonly Guid GUID_DEVCLASS_USB = new Guid("{36fc9e60-c465-11cf-8056-444553540000}");

        private static ManagementScope CIMv2 = new ManagementScope("root\\CIMV2");
        private ManagementEventWatcher genericWatcher = new ManagementEventWatcher(CIMv2, new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 2 OR EventType = 3"));

        private HashSet<string> pnpEntityList = null;
        private object genericLock = new object();

        EventArrivedEventHandler genericEventHandler;

        ///<summary>
        /// Creates an instance of the <see cref="WindowsDeviceNotifier"/> class.
        /// See the <see cref="IDeviceNotifier"/> interface or <see cref="DeviceNotifier.OpenDeviceNotifier"/> method for more information
        ///</summary>
        ///<remarks>
        ///To make your code platform-independent use the <see cref="DeviceNotifier.OpenDeviceNotifier"/> method for creating instances.
        ///</remarks>
        public WindowsManagementUSBDeviceNotifier() {
            Enabled = true;
        }
        private void DeleteWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            if (!mEnabled) return;
            ManagementBaseObject instance = (ManagementBaseObject)e.NewEvent["TargetInstance"];
            if (instance == null || instance["ClassGuid"] == null || instance["DeviceId"] == null) return;
            OnDeviceChange(instance["DeviceId"] as string, EventType.DeviceRemoveComplete);
        }

        private void InsertWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            if (!mEnabled) return;
            ManagementBaseObject instance = (ManagementBaseObject)e.NewEvent["TargetInstance"];
            if (instance == null || instance["ClassGuid"] == null || instance["DeviceId"] == null) return;

            OnDeviceChange(instance["DeviceId"] as string, EventType.DeviceArrival);
        }

        #region IDeviceNotifier Members

        ///<summary>
        /// Enables/Disables notification events.
        ///</summary>
        public bool Enabled
        {
            get {
                return mEnabled;
            }
            set {
                if (mEnabled == value)
                    return;

                mEnabled = value;

                try {
                    if (value) {
                        lock (genericLock) {
                            if (pnpEntityList == null)
                                pnpEntityList = GetPnPEntities();
                        }
                        genericEventHandler = new EventArrivedEventHandler(GenericWatcher_EventArrived);
                        genericWatcher.EventArrived += genericEventHandler;
                        genericWatcher.Start();
                    } else {
                        genericEventHandler -= genericEventHandler;
                        if (genericWatcher != null) {
                            genericWatcher.Stop();
                            genericWatcher.Dispose();
                            genericWatcher = null;
                        }
                    }
                } catch (Exception) {
                    mEnabled = false;
                }
            }
        }

        private HashSet<string> GetPnPEntities()
        {
            HashSet<string> currentDevices = new HashSet<string>();
            string query = "SELECT deviceId FROM Win32_PnPEntity WHERE ClassGuid=\"{36fc9e60-c465-11cf-8056-444553540000}\"";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(CIMv2, new WqlObjectQuery(query));
            foreach (ManagementObject usbDevice in searcher.Get())
            {
                string deviceId = usbDevice.Properties["DeviceId"].Value.ToString();
                if (!deviceId.StartsWith("USB"))
                    continue;

                currentDevices.Add(usbDevice.Properties["DeviceId"].Value.ToString());
            }

            return currentDevices;
        }

        private void TestCollection(PropertyDataCollection pdc)
        {
            foreach (PropertyData prop in pdc)
            {
                Console.WriteLine(prop.Name+" ["+prop.Type+"]: "+prop.Value);
            }
        }

        private void GenericWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            try {
                lock (genericLock) {
                    HashSet<string> currentDevices = GetPnPEntities();
                    HashSet<string> addedDevices = new HashSet<string>(currentDevices);
                    addedDevices.ExceptWith(pnpEntityList);

                    HashSet<string> removedDevices = new HashSet<string>(pnpEntityList);
                    removedDevices.ExceptWith(currentDevices);

                    if (addedDevices.Count > 0) {
                        foreach (string device in addedDevices)
                            OnDeviceChange(device, EventType.DeviceArrival);
                    }

                    if (removedDevices.Count > 0)
                    {
                        foreach (string device in removedDevices)
                            OnDeviceChange(device, EventType.DeviceRemoveComplete);
                    }

                    pnpEntityList = currentDevices;
                }
            } catch (Exception er) {
                Console.Write(er);
            }
        }



        /// <summary>
        /// Main Notify event for all device notifications.
        /// </summary>
        public event EventHandler<DeviceNotifyEventArgs> OnDeviceNotify;

        #endregion

        ///<summary>
        ///Releases the resources associated with this window. 
        ///</summary>
        ///
        ~WindowsManagementUSBDeviceNotifier()
        {
            Enabled = false;
        }

        string GetLocation(string deviceId)
        {
            string location = null;
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum\\" + deviceId))
            {
                if (key != null)
                    location = key.GetValue("LocationInformation") as string;
            }

            if (location != null)
                return location;

            string query = string.Format(@"SELECT * FROM Win32_PnPSignedDriver WHERE DeviceId=""{0}""", deviceId.Replace("\\", "\\\\"));
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(CIMv2, new WqlObjectQuery(query));
            foreach (ManagementObject usbDevice in searcher.Get())
                return usbDevice.Properties["Location"].Value as string;

            return null;
        }

        private void OnDeviceChange(string deviceId, EventType eventType)
        {
            EventHandler<DeviceNotifyEventArgs> temp = OnDeviceNotify;
            try {
                DeviceNotifyEventArgs args = new WindowsManagementUSBDeviceNotifyArgs("USB Printing Support", deviceId, GUID_DEVCLASS_USB.ToString(), eventType == EventType.DeviceRemoveComplete ? null : GetLocation(deviceId), eventType);
                if (!ReferenceEquals(args, null)) temp(this, args);
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }
    }
} 