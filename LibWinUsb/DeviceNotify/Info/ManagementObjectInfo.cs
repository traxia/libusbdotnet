﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibUsbDotNet.Main;
using System.Management;

namespace LibUsbDotNet.DeviceNotify.Info
{
    class ManagementObjectInfo : IUsbDeviceNotifyInfo
    {
        public static ManagementObjectInfo Create(string name, string deviceId, string guid, string location)
        {
            ManagementObjectInfo moi = new ManagementObjectInfo();

            try {
                ParseUsingBase(name, deviceId, guid, ref moi);
                if (location != null)
                    moi.Location = location;
            } catch (Exception) { return null; }

            return moi;
        }

        private static void ParseUsingBase(string name, string deviceId, string guid, ref ManagementObjectInfo moi)
        {
            moi.ClassGuid = Guid.Parse(guid);

            string DeviceId = deviceId;
            UsbSymbolicName usbSymbolicName = new UsbSymbolicName(DeviceId);
            moi.SymbolicName = usbSymbolicName;
            moi.IdVendor = usbSymbolicName.Vid;
            moi.IdProduct = usbSymbolicName.Pid;

            try
            {
                string[] idSplit = DeviceId.Split('\\');
                moi.SerialNumber = idSplit[idSplit.Length - 1];
            }
            catch (Exception)
            {
                moi.SerialNumber = usbSymbolicName.SerialNumber;
            }

            moi.Name = name;
        }

        protected ManagementObjectInfo() {}

        public Guid ClassGuid
        {
            private set; get;
        }

        public int IdProduct
        {
            private set; get;
        }

        public int IdVendor
        {
            private set; get;
        }

        public string Name
        {
            private set; get;
        }

        public string SerialNumber
        {
            private set; get;
        }

        public UsbSymbolicName SymbolicName
        {
            private set; get;
        }

        public string Location
        {
            private set; get;
        }
    }
}
